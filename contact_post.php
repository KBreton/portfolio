<?php
/**
 * Created by PhpStorm.
 * User: Katherine
 * Date: 19/01/19
 * Time: 13:19
 */

if(!empty($_POST['lname']) AND !empty($_POST['mail']) AND !empty($_POST['textarea'])){
    if(preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['mail']) AND isset($_POST['lname']) AND isset($_POST['textarea'])){

        $name = htmlspecialchars($_POST['lname']);
        $mail = htmlspecialchars($_POST['mail']);
        $message = htmlspecialchars($_POST['textarea']);

        /* Script mail */
        $to = 'katherine.breton@viacesi.fr';
        $subject = 'Formulaire de contact';

        mail($to, $subject,
            'Le message du destinataire: '. $message .
            ' Son nom: '. $name .
            ' Son adresse mail: '. $mail);

    }else{
        echo "Le format de l'adresse mail n'est pas valide";
    }
}else{
    echo "Tous les champs doivent être remplis.";
}
header("Location: Contact.php");
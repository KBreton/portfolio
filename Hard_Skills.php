<?php
include ("head.php");
?>

<body id="hardskills">
<?php include("navbar.php");?>
    <h2>Hard Skills</h2>
    <div id="hs">
        <div class="competences">
            <h4>HTML5</h4>
                <p>Création de structure HTML et gestion du contenu, formulaires, respect de la sémantique</p>
                <div class="meter">
                    <span class="progress" style="width: 80%; background-color: rgb(39,52,194)"></span>
                </div>
            <h4>CSS3</h4>
                <p>Mise en page CSS, propriétés Flexbox et Grid, Responsive Design, préprocesseurs LESS et SASS</p>
                <div class="meter">
                    <span class="progress" style="width: 65%; background-color: rgb(74,181,194);"></span>
                </div>
            <h4>PHP</h4>
                <p>Lecture et écriture dans une base de données, utilisation des variables globales, utilisation d'
                expressions régulières, initiation à l'architecture MVC</p>
                <div class="meter">
                    <span class="progress" style="width: 25%;   background-color: rgb(39,52,194);"></span>
                </div>
            <h4>SQL</h4>
                <p>Application des opérations d'algèbre relationnelle au requêtes SQL, écriture de requêtes SQL</p>
                <div class="meter">
                    <span class="progress" style="width: 30%;   background-color: rgb(74,181,194);"></span>
                </div>
            <h4>Langage C</h4>
                <p>Algorithmique, programmation console, utilisation des structures itératives, pointeurs, fonctions,
                développement de petits programmes</p>
                <div class="meter">
                    <span class="progress" style="width: 25%;   background-color: rgb(39,52,194);"></span>
                </div>
            <h4>JavaScript</h4>
                <p>Syntaxe JavaScript, résolution de problèmes de base, initiation à l'animation de pages web</p>
                <div class="meter">
                    <span class="progress" style="width: 25%;   background-color: rgb(74,181,194);"></span>
                </div>
            <h4>Photoshop</h4>
                <p>Apprentissage autodidacte depuis 13 ans principalement pour le dessin et le design</p>
                <div class="meter">
                    <span class="progress" style="width: 45%;   background-color: rgb(39,52,194);"></span>
                </div>
            <h4>Méthode Merise</h4>
            <p>Analyse et conceptualisation d'un problème pour la modélisation d'une base de données</p>
            <div class="meter">
                    <span class="progress" style="width: 35%;   background-color: rgb(74,181,194);"></span>
                </div>
            <h4>Linux</h4>
            <p>Gestion des utilisateurs et des droits, connexion SSH à distance, installation et configuration d'un LAMP sous Debian</p>
            <div class="meter">
                    <span class="progress" style="width: 35%;   background-color: rgb(39,52,194);"></span>
                </div>
            <h4>Git</h4>
            <p>Enregistrer et gérer son historique de code avec les commits, gérer un repository, résoudre les conflits de branche</p>
            <div class="meter">
                <span class="progress" style="width: 50%;   background-color: rgb(74,181,194);"></span>
            </div>
            </div>
        <div class="oc">
            <h3>Open Classrooms</h3>
            <h4>Obtenus</h4>
                <div id="grid">
                    <a href="https://openclassrooms.com/fr/courses/4449026-initiez-vous-a-lalgebre-relationnelle-avec-le-langage-sql" target="_blank"><img class="certif" src="images/certif1.jpg" alt="algèbre relationnelle et SQL"></a>
                    <a href="https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql" target="_blank"><img class="certif" src="images/certif2.jpg" alt="PHP MySQL"></a>
                    <a href="https://openclassrooms.com/fr/courses/1946386-comprendre-le-web" target="_blank"><img class="certif" src="images/certif3.jpg" alt="Comprendre le web"></a>
                    <a href="https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3" target="_blank"><img class="certif" src="images/certif4.jpg" alt="HTML5 CSS3"></a>
                    <a href="https://openclassrooms.com/fr/courses/2984401-apprenez-a-coder-avec-javascript" target="_blank"><img class="certif" src="images/certif5.jpg" alt="Apprenez à coder avec JavaScript"></a>
                    <a href="https://openclassrooms.com/fr/courses/2342361-gerez-votre-code-avec-git-et-github" target="_blank"><img class="certif" src="images/certif6.jpg" alt="Gérez votre code avec Git et GitHub"></a>
                </div>
            <h4>En cours</h4>
            <a href="https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c" target="_blank"><h5>Apprenez à programmer en C !</h5></a>
            <div class="meter">
                <span class="progress" style="width: 35%;   background-color: rgb(49,194,71);"></span>
            </div>
            <a href="https://openclassrooms.com/fr/courses/4670706-adoptez-une-architecture-mvc-en-php" target="_blank"><h5>Adoptez une architecture MVC en PHP</h5></a>
            <div class="meter">
                <span class="progress" style="width: 72%;   background-color: rgb(49,194,71);"></span>
            </div>
            <a href="https://openclassrooms.com/fr/courses/3306901-creez-des-pages-web-interactives-avec-javascript" target="_blank"><h5>Créez des pages interactives avec JavaScript</h5></a>
            <div class="meter">
                <span class="progress" style="width: 12%;   background-color: rgb(49,194,71);"></span>
            </div>
            <a href="https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux" target="_blank"><h5>Reprenez le contrôle à l'aide de Linux ! </h5></a>
            <div class="meter">
                <span class="progress" style="width: 38%;   background-color: rgb(49,194,71);"></span>
            </div>
            <a href="https://openclassrooms.com/fr/courses/4975451-demarrez-votre-projet-avec-java" target="_blank"><h5>Démarrez votre projet avec Java </h5></a>
            <div class="meter">
                <span class="progress" style="width: 28%;   background-color: rgb(49,194,71);"></span>
            </div>
        </div>
    </div>


</body>

</html>

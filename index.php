<?php include ("head.php");?>

<body id="indexback">
<?php include("navbar.php");?>
    <div id="index">
        <div id="intro">
            <picture>
                <img src="images/Profil1.png" alt="Ma photo de profil">
            </picture>
            <h1>Katherine Breton</h1>
            <p>Développeuse informatique en recherche d'alternance avec le Cesi</p>
            <ul id="contactlist">
                <li><a href="https://www.linkedin.com/in/katherine-breton/" target="_blank"><img src="logo/linkedin.png" alt="LinkedIn"></a></li>
                <li><a href="mailto:katherine.breton@viacesi.fr"><img src="logo/mail.png" alt="Mail"></a></li>
                <li id="myBtn"><img src="logo/phone.png" alt="Téléphone"></li>
                <li><a href="https://gitlab.com/KBreton" target="_blank"><img src="logo/gitlab.png" alt="Twitter"></a></li>
            </ul>
            <div>
                <a href="CV_Katherine_BRETON.pdf" target="_blank">Mon CV</a>
                <a href="Développeur_Informatique.pdf" target="_blank">En savoir plus</a>
            </div>

            <!-- Le pop up une fois ouvert -->
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <p>06 68 02 89 47</p>
                </div>
            </div>

        </div>
    </div>
<script>
    // Récup du modal
    var modal = document.getElementById("myModal");

    // Récup du bouton qui ouvre le modal
    var btn = document.getElementById("myBtn");

    // Récup du <span> qui ferme le modal
    var span = document.getElementsByClassName("close")[0];

    // Quand l'utilisateur clique, ouvre le modal
    btn.onclick = function() {
        //change le style du modal sur visibility: visible;
        modal.style.visibility = "visible";
    }

    // Quand l'utilisateur clique sur le <span> (x), ferme le modal
    span.onclick = function() {
        //change le style du modal sur visibility: hidden;
        modal.style.visibility = "hidden";
    }

    // Quand l'utilisateur clique ailleurs que sur le modal, le fermer
    window.onclick = function(event) {
        if (event.target === modal) {
            //change le style du modal sur visibility: hidden;
            modal.style.visibility = "hidden";
        }
    }

</script>
</body>

</html>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Katherine BRETON | Étudiante en développement informatique </title>
    <meta charset="utf-8">
    <meta name="description" content="Je suis étudiante en développement informatique et je recherche une entreprise où
          je pourrai poursuivre ma formation en alternance."  />
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=yes"  />

    <link rel="stylesheet/less" type="text/css" href="css_less/stylesheet.less" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="manifest" href="manifest.json"  />
    <link rel="apple-touch-icon" href="icon.png"  />
    <link rel="icon" href="logo/favicon.ico" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        less.env="development";
        less.watch();
    </script>
</head>

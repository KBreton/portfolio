<?php
include ("head.php");
?>

<body id="description">
<?php include("navbar.php");?>
    <h2>Qui suis-je?</h2>
    <div id="text">
        <div class="para">

            <p> D’origine Québécoise, je suis installée en France depuis 2011.  Je me passionne pour l'informatique et les nouvelles
            technologies depuis mon enfance, en ayant depuis longtemps le projet d'en faire mon métier.
            </p>

            <h3>Mon parcours</h3>
            <p>J'ai fait la plus grande partie de mes études au Québec, avec un diplôme d'études secondaires obtenu en 2009, qui
                équivaut à un diplôme de fin d'études. J'ai alors entamé une formation en animation 3D au Cégep du Vieux-Montréal
                que je n'ai pu terminer, à regret. Je me suis alors tournée vers ma seconde passion, la cuisine, plus précisément
                en pâtisserie française avec un diplôme en poche à l'issue de 18 mois de formation et l'envie de me former
                davantage au cours d'un voyage en France.
            </p>

            <p>Après avoir travaillé plusieurs années dans la pâtisserie et la restauration, l'envie de reprendre des études dans
                le milieu de l'informatique grandissait de plus en plus en moi.  J'ai donc fait un bilan de compétences Fongecif afin
                de valider ce projet, et j'ai décidé de m'inscrire en développement informatique au Cesi.
            </p>

            <p>Je me forme dans des langages informatiques variés, et j'aimerais poursuivre mon apprentissage dans un milieu
            professionnalisant, et dans un cadre dynamique.  Je recherche donc activement une entreprise qui pourra me permettre
            d'en apprendre plus dans ce domaine passionnant tout en me permettant d'apporter ma contribution dans les projets
            auquels je serai amenée à participer.  J'aime les nouveaux défis et j'ai à coeur d'apprendre encore et encore.
            </p>

            <h3>Mes passions</h3>
            <p>Sur une note plus personnelle, mes centres d'intérêts sont, au-delà de l'informatique, la cuisine, l'actualité
                et une constante envie d'apprendre. Enfin, je me passionne également pour la nature, que ce soit des balades
                en forêt ou la réalisation d'un potager, et comme projet d'ici à 2020, l'apprentissage de l'apiculture.</p>

        </div>
    </div>
</body>

</html>

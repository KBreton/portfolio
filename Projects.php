<?php
include ("head.php");
?>
<body id="projects">
<?php include("navbar.php");?>

        <h2>Projets</h2>

        <div id="all">
        <div id="current">
            <h3>En cours</h3>
                <h4>Refonte du site <a href="https://super7.fr" target="_blank">super7.fr</a></h4>
                <img class="screen" src="images/Super7.PNG" alt="Page d'accueil ancien site Super7.fr">
                <p>Je travaille actuellement sur un projet client en équipe de trois pour le site de l’association super7.
                    Nous devons procéder à une refonte complète du site et changer la charte graphique, intégrer un système de newsletter,
                    un système de curation et intégrer les réseaux sociaux pour rendre le site plus vivant.  Nous migrons également
                    le site de Drupal 7 à Drupal 8.
                </p>
            <figure class="logo">
                <img src="logo/drupal.png" alt="logo Drupal">
                <img src="logo/html-5.png" alt="logo html5">
                <img src="logo/css.png" alt="logo css3">
                <img src="logo/php.png" alt="logo php">
                <img src="logo/mysql.png" alt="logo mysql">
            </figure>
        </div>

        <div id="past">
            <h3>Passés</h3>

            <h4>Projet PHP refonte du site "Yvan l'alternant"</h4>
            <img class="screen" src="images/Yvanv2.PNG" alt="Page d'accueil du site Yvan l'alternant">
            <img class="screen" src="images/Yvanv2-1.PNG" alt="Code du site Yvan l'alternant">
            <p>Projet en équipe de quatre, nous avons repris un projet que nous avions fait au mois de décembre pour le refaire
                entièrement suite aux cours PHP.  Le but du site est de permettre un matching entre des entreprises et des
                étudiants cherchant un stage ou une alternance.  Un de mes coéquipiers a un excellent niveau en PHP et
                a partagé ses connaissances avec le reste de l'équipe, notamment au niveau de l'architecture MVC.
            </p>
            <figure class="logo">
                <img src="logo/html-5.png" alt="logo html5">
                <img src="logo/css.png" alt="logo css3">
                <img src="logo/php.png" alt="logo php">
                <img src="logo/mysql.png" alt="logo mysql">
                <img src="logo/javascript.png" alt="logo javascript">
            </figure>
                <h4>Blog en PHP</h4>
            <img class="screen" src="images/Blog.PNG" alt="Apparence globale du blog en PHP">
            <img class="screen" src="images/Blog2.PNG" alt="Code du blog en PHP">
            <p>Petit projet individuel durant notre apprentissage des langages PHP et MySQL.  Nous devions mettre en place un site de
                    type blog, dans lequel nous pouvions, une fois un profil créé et une session ouverte, poster des articles.  Nous
                    devions ensuite intégrer la possibilité de les modifier ou de les effacer.  J'ai appliqué quelques propriétés CSS
                pour qu'il soit plus agréable visuellement qu'avec un HTML seul, mais cet aspect n'était pas obligatoire.
                </p>
            <figure class="logo">
                <img src="logo/html-5.png" alt="logo html5">
                <img src="logo/css.png" alt="logo css3">
                <img src="logo/php.png" alt="logo php">
                <img src="logo/mysql.png" alt="logo mysql">
            </figure>

                <h4>Projet libre "Yvan l'alternant"</h4>
            <img class="screen" src="images/Yvan.PNG" alt="Page d'accueil du site Yvan l'alternant">
            <img class="screen" src="images/Yvan2.PNG" alt="Code du site Yvan l'alternant">
                <p>Projet en équipe de quatre, sur un thème libre, nous avons choisi de faire un site web à mi-chemin entre le site
                    de rencontre et le site de recherche d’emploi axé sur la recherche d’alternance et/ou de stage.  Ce projet m’a
                    permis de me pencher sur le langage PHP pour la première fois et de me familiariser avec sa syntaxe.  J’ai
                    principalement travaillé sur l’aspect du Front End et sur la réalisation du modèle conceptuel des données pour la base de données.
                </p>
            <figure class="logo">
                <img src="logo/html-5.png" alt="logo html5">
                <img src="logo/css.png" alt="logo css3">
                <img src="logo/php.png" alt="logo php">
                <img src="logo/mysql.png" alt="logo mysql">
                <img src="logo/javascript.png" alt="logo javascript">
            </figure>

                <h4>VBA Excel</h4>
            <img class="screen" src="images/Excel.PNG" alt="Apparence du programme">
            <img class="screen" src="images/Excel1.PNG" alt="Code VBA">
                <p>Projet individuel consistant à importer une médiathèque au format .txt dans des feuilles de calcul Excel avec
                    une page de description par films et la possibilité de rechercher selon certains critères (genre, année, etc.).
                    J'ai codé en Visual Basic et utilisé les macros.
            <figure class="logo">
                <img src="logo/excel.png" alt="logo excel">
            </figure>


                <h4>Portfolio</h4>
            <img class="screen" src="images/Portfolio.PNG" alt="Page d'accueil Portfolio">
            <img class="screen" src="images/Portfolio1.PNG" alt="Code PHP/HTML5/CSS3 Portfolio">
                <p>	Projet individuel de mise ne place d’un portfolio en ligne pour notre recherche d'alternance.  Le but était
                    de mettre en pratique nos connaissances des langages web et d'étoffer ce site au fur et à mesure de notre apprentissage.
                    J'ai fait ce site à partir de zéro, sans framework, afin d'utiliser plusieurs notions apprises en cours ou sur internet.
                </p>
            <figure class="logo">
                <img src="logo/html-5.png" alt="logo html5">
                <img src="logo/css.png" alt="logo css3">
                <img src="logo/php.png" alt="logo php">
                <img src="logo/mysql.png" alt="logo mysql">
                <img src="logo/javascript.png" alt="logo javascript">
            </figure>

                <h4>Projet en langage C</h4>
            <img class="screen" src="images/ProjetC.PNG" alt="Code du programme">
                <p>	Projet en équipe de trois, où nous devions créer un programme capable d’importer et de trier des items listés
                    dans un fichier .csv vers un .txt.  Je n'avais jamais codé en langage C auparavant et ce projet m'a permis de mettre en pratique
                    les notions de fonctions et de pointeurs en C.
                </p>
            <figure class="logo">
            <img src="logo/c.png" alt="logo C">
            </figure>

                <h4>Breaking Ice Mockups</h4>
            <img class="screen" src="images/Balsamiq.PNG" alt="Mockups Balsamiq">
                <p>	Projet en équipe de quatre, pour apprendre à se connaître entre étudiants.  Nous devions créer des mockups, à l'aide
                    de Balsamiq Mockups, d'une application fictive servant à faire connaître la vie d'un développeur informatique.  Nous avons
                    
                </p>
        </div>

    </div>

</body>

</html>
